<!doctype html>
<html>
  <head>
    <title>Bazinger - главная страница</title>
    <link rel="shortcut icon" href="#" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,700i,900" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="assets/js/slick/slick-theme.css"/> 
    <link rel="stylesheet" type="text/css" href="assets/js/fancybox/jquery.fancybox.min.css"/> 
    <link rel="stylesheet" href="assets/css/styles.css">
    <meta charset="utf-8"> 
    <meta name="vieport" content="width=device-width"> 
  </head>
  <body>
    <header class="site-header">
      <div class="container">
        <a href="/" class="logo">
          <img src="assets/img/logo.png" alt="">                    
        </a>
        <nav class="site-header__menu main-menu">
          <ul class="menu">
            <li><a href="?page=home">HOME</a>
              <ul>
                <li><a href="#">Пункт 1</a></li>
                <li><a href="#">Пункт 2</a></li>
                <li><a href="#">Пункт 3</a></li>                
              </ul>
            </li>
            <li><a href="?page=features">FEATURES</a>
              <ul>
                <li><a href="#">Пункт 1</a></li>
                <li><a href="#">Пункт 2</a></li>
                <li><a href="#">Пункт 3</a></li>                
              </ul>
            </li>
            <li><a href="?page=gallery">GALLERY</a>
              <ul>
                <li><a href="#">Пункт 1</a></li>
                <li><a href="#">Пункт 2</a></li>
                <li><a href="#">Пункт 3</a></li>                
              </ul>
            </li>
            <li><a href="?page=video">VIDEO</a>
              <ul>
                <li><a href="#">Пункт 1</a></li>
                <li><a href="#">Пункт 2</a></li>
                <li><a href="#">Пункт 3</a></li>                
              </ul>                      
            </li>
            <li><a href="?page=prices">PRICES</a>
              <ul>
                <li><a href="#">Пункт 1</a></li>
                <li><a href="#">Пункт 2</a></li>
                <li><a href="#">Пункт 3</a></li>                
              </ul>
            </li>
            <li><a href="?page=testimonials">TESTIMONIALS</a>
              <ul>
                <li><a href="#">Пункт 1</a></li>
                <li><a href="#">Пункт 2</a></li>
                <li><a href="#">Пункт 3</a></li>                
              </ul>
            </li>
            <li><a href="?page=download">DOWNLOAD</a>
              <ul>
                <li><a href="#">Пункт 1</a></li>
                <li><a href="#">Пункт 2</a></li>
                <li><a href="#">Пункт 3</a></li>                
              </ul>
            </li>
            <li><a href="?page=contact">CONTACT</a>
              <ul>
                <li><a href="#">Пункт 1</a></li>
                <li><a href="#">Пункт 2</a></li>
                <li><a href="#">Пункт 3</a></li>                
              </ul>
            </li>
          </ul>          
        </nav>        
      </div>
    </header>
    <div class="slider-wrapper">      
      <div class="home_firstscreen">
        <div class="container">      
          <ul class="slider">
            <li class="slide">     
              <div class="slide-wrapper">           
                <div class="first-slider-title">Simple, Beautiful <span>and Amazing</span></div>
                <div class="slider-description">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget nunc vitae tellus luctus ullamcorper. Nam porttitor ullamcorper felis at convallis. Aenean ornare vestibulum nisi fringilla lacinia. Nullam pulvinar sollicitudin velit id laoreet. Quisque non rhoncus sem.
                </div>
                <a href="#" class="button slider-download">DOWNLOAD</a>  
                <a href="#" class="button learn-more">LEARN MORE</a>
                <div class="available-block">
                  <div class="available-on">Available on:</div>
                  <a href="#"><img src="assets/img/apple-icon.png"></a>
                  <a href="#"><img src="assets/img/andr-icon.png"></a>
                </div>
              </div>
            </li>  
            <li class="slide">     
              <div class="slide-wrapper">           
                <div class="first-slider-title">Simple, Beautiful <span>and Amazing</span></div>
                <div class="slider-description">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget nunc vitae tellus luctus ullamcorper. Nam porttitor ullamcorper felis at convallis. Aenean ornare vestibulum nisi fringilla lacinia. Nullam pulvinar sollicitudin velit id laoreet. Quisque non rhoncus sem.
                </div>
                <a href="#" class="button slider-download">DOWNLOAD</a>  
                <a href="#" class="button learn-more">LEARN MORE</a>
                <div class="available-block">
                  <div class="available-on">Available on:</div>
                  <a href="#"><img src="assets/img/apple-icon.png"></a>
                  <a href="#"><img src="assets/img/andr-icon.png"></a>
                </div>
              </div>
            </li>  
            <li class="slide">     
              <div class="slide-wrapper">           
                <div class="first-slider-title">Simple, Beautiful <span>and Amazing</span></div>
                <div class="slider-description">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget nunc vitae tellus luctus ullamcorper. Nam porttitor ullamcorper felis at convallis. Aenean ornare vestibulum nisi fringilla lacinia. Nullam pulvinar sollicitudin velit id laoreet. Quisque non rhoncus sem.
                </div>
                <a href="#" class="button slider-download">DOWNLOAD</a>  
                <a href="#" class="button learn-more">LEARN MORE</a>
                <div class="available-block">
                  <div class="available-on">Available on:</div>
                  <a href="#"><img src="assets/img/apple-icon.png"></a>
                  <a href="#"><img src="assets/img/andr-icon.png"></a>
                </div>
              </div>
            </li>                        
          </ul>                                  
        </div> 
      </div>  
    </div> 
    <?php  
      include_once "config.php";      
      $link = mysqli_connect($db_host, $db_user, $db_password, $db_base) or die('Ошибка' . mysqli_error($link));

      if ( $_GET['page'] == 'features' ) {
          $query_select = 'SELECT `content` FROM `html_content` WHERE `name` = "features"';
          $result = mysqli_query($link, $query_select) or die('Ошибка' . mysqli_error($link));
          while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo $row['content'];
          }      
          mysqli_free_result($result);  
      } else if ( $_GET['page'] == 'gallery' ) {
          $query_select = 'SELECT `content` FROM `html_content` WHERE `name` = "gallery"';
          $result = mysqli_query($link, $query_select) or die('Ошибка' . mysqli_error($link));
          while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo $row['content'];
          }      
          mysqli_free_result($result);   
      } else if ( $_GET['page'] == 'video' ) {
          $query_select = 'SELECT `content` FROM `html_content` WHERE `name` = "video"';
          $result = mysqli_query($link, $query_select) or die('Ошибка' . mysqli_error($link));
          while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo $row['content'];
          }      
          mysqli_free_result($result);   
      } else if ( $_GET['page'] == 'prices' ) {
          $query_select = 'SELECT `content` FROM `html_content` WHERE `name` = "prices"';
          $result = mysqli_query($link, $query_select) or die('Ошибка' . mysqli_error($link));
          while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo $row['content'];
          }      
          mysqli_free_result($result);  
      } else if ( $_GET['page'] == 'testimonials' ) {
          $query_select = 'SELECT `content` FROM `html_content` WHERE `name` = "testimonials"';
          $result = mysqli_query($link, $query_select) or die('Ошибка' . mysqli_error($link));
          while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo $row['content'];
          }      
          mysqli_free_result($result);  
      } else if ( $_GET['page'] == 'download' ) {
          $query_select = 'SELECT `content` FROM `html_content` WHERE `name` = "download"';
          $result = mysqli_query($link, $query_select) or die('Ошибка' . mysqli_error($link));
          while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo $row['content'];
          }      
          mysqli_free_result($result);   
      } else if ( $_GET['page'] == 'contact' ) {
          $query_select = 'SELECT `content` FROM `html_content` WHERE `name` = "contact"';
          $result = mysqli_query($link, $query_select) or die('Ошибка' . mysqli_error($link));
          while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo $row['content'];
          }      
          mysqli_free_result($result);  
      } else {                 
          $query_select = 'SELECT `content` FROM `html_content` WHERE `name` = "home"';
          $result = mysqli_query($link, $query_select) or die('Ошибка' . mysqli_error($link));
          while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            echo $row['content'];
          }      
          mysqli_free_result($result);          
      }
     ?>
 
    <!-- <div class="advantages">
      <div class="container">
        <div class="titles">
          <div class="main_title">summarise the features</div>
          <div class="second_title">summarise what your product is all about</div>
        </div>   
        <div class="advantages_elements">
          <div class="row">
          <div class="col-3">
            <div class="advantage layout">              
              <div class="advantage-img">                
                <img src="assets/img/icon-layout.png">
              </div>
              <div class="advantage_title">Attractive Layout</div>
              <div class="advantage_description">
                Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar.
              </div>
            </div> 
          </div>
          <div class="col-3">
            <div class="advantage design">
              <div class="advantage-img">
                <img src="assets/img/icon-design.png">
              </div>
              <div class="advantage_title">Fresh Design</div>
              <div class="advantage_description">
                Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar.
              </div>
            </div> 
          </div>
          <div class="col-3">
            <div class="advantage purpose">
              <div class="advantage-img">
                <img src="assets/img/icon-purpose.png">
              </div>
              <div class="advantage_title">multipurpose</div>
              <div class="advantage_description">
                Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar.
              </div>
            </div> 
          </div>
          <div class="col-3">
            <div class="advantage customize">
              <div class="advantage-img">
                <img src="assets/img/icon-customize.png">
              </div>
              <div class="advantage_title">Easy to customize</div>
              <div class="advantage_description">
                Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar.
              </div>
            </div> 
          </div>
        </div> 
        </div>
      </div>
    </div>
    <div class="gallery">
      <div class="container">
        <div class="titles">
          <div class="main_title">show the gallery</div>
          <div class="second_title">summarise what your product is all about</div>
        </div>  
        <div class="gallery-elements">
          <div class="row">
            <div class="col-3">              
              <div class="gallery-element screen1">
                <div class="element-img">
                  <a class="fancybox" data-fancybox="gallery" href="img/screen1.jpg"><img src="img/screen1.jpg" alt="" /></a>                          
                  <div class="element-name">SCREEN SHOT #1</div>
                </div>
                <div class="element-description">
                  Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar. 
                </div>   
              </div>
            </div> 
            <div class="col-3">
              <div class="gallery-element screen2">
                <div class="element-img">   
                  <a class="fancybox" data-fancybox="gallery" href="img/screen2.jpg"><img src="img/screen2.jpg" alt="" /></a>               
                  <div class="element-name">SCREEN SHOT #2</div>
                </div>
                <div class="element-description">
                  Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar. 
                </div>   
              </div>   
            </div>
            <div class="col-3">
              <div class="gallery-element screen3">
                <div class="element-img">    
                  <a class="fancybox" data-fancybox="gallery" href="img/screen3.jpg"><img src="img/screen3.jpg" alt="qqq" /></a>              
                  <div class="element-name">SCREEN SHOT #3</div>
                </div>
                <div class="element-description">
                  Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar. 
                </div>   
              </div> 
            </div>
            <div class="col-3">
              <div class="gallery-element screen4">
                <div class="element-img">
                  <a class="fancybox" data-fancybox="gallery" href="img/screen4.jpg"><img src="img/screen4.jpg" alt="" /></a>                   
                  <div class="element-name">SCREEN SHOT #4</div>
                </div>
                <div class="element-description">
                  Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam elerisque mi id faucibus iaculis vitae pulvinar. 
                </div>   
              </div>
            </div>        
          </div>   
        </div>        
      </div>
    </div>
    <div class="video-box">
      <div class="start-screen">
        <div class="video-button">
          <img src="assets/img/video-button.png">         
        </div>
        <div class="video-title">Watch the best Technology in <span>Action</span></div>
        <div class="video-description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eget nunc vitae tellus luctus ullamcorper. Nam porttitor ullamcorper felis at convallis. Aenean ornare vestibulum nisi fringilla lacinia. Nullam pulvinar sollicitudin velit id laoreet. Quisque non rhoncus sem.
        </div>
      </div>  
    </div>
    <div class="choosing-area">
        <div class="container">
          <div class="titles">
            <div class="main_title">choose your price</div>
            <div class="second_title">summarise what your product is all about</div>
          </div> 
          <div class="package-area">
            <div class="package basic">
              <div class="package-name">basic package</div>
              <div class="package-price">20$</div>
              <div class="package-description">
                Nullam suscipit vitae
                Etiam faucibus
                Vivamus viverra
              </div>
              <a href="#" class="package-button">PURCHASE</a>
            </div>
            <div class="package professional">
              <div class="package-name">professional package</div>
              <div class="package-price">25$</div>
              <div class="package-description">
                Nullam suscipit vitae
                Etiam faucibus
                Vivamus viverra
                Praesent pharetra
              </div>
              <a href="#" class="package-button">PURCHASE</a>
            </div>
            <div class="package advanced">
              <div class="package-name">advanced package</div>
              <div class="package-price">30$</div>
              <div class="package-description">
                Nullam suscipit vitae
                Etiam faucibus
                Vivamus viverra            
              </div>
              <a href="#" class="package-button">PURCHASE</a>
            </div>
          </div> 
        </div>   
    </div>
    <div class="second_slider-area">
      <div class="container">
        <div class="slider-title">Testimonials</div>
        <div class="second_slider-wrapper">
            <ul class="slider">
              <li class="slide"> 
                <div class="slide-wrapper">
                  <img src="assets/img/icon-silhouette.png">
                  <div class="comments">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                      Praesent lobortis lectus eget libero blandit venenatis. 
                      In blandit tortor vel congue malesuada. Suspendisse 
                      molestie lobortis lorem dignissim pretium.                    
                      <span class="signature-name">John Doe</span>                    
                      <span class="signature-from">from some company</span>
                  </div>
                </div>               
              </li> 
              <li class="slide"> 
                <div class="slide-wrapper">
                  <img src="assets/img/icon-silhouette.png">
                  <div class="comments">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                      Praesent lobortis lectus eget libero blandit venenatis. 
                      In blandit tortor vel congue malesuada. Suspendisse 
                      molestie lobortis lorem dignissim pretium.                    
                      <span class="signature-name">John Doe</span>                    
                      <span class="signature-from">from some company</span>
                  </div>
                </div>               
              </li> 
              <li class="slide"> 
                <div class="slide-wrapper">
                  <img src="assets/img/icon-silhouette.png">
                  <div class="comments">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                      Praesent lobortis lectus eget libero blandit venenatis. 
                      In blandit tortor vel congue malesuada. Suspendisse 
                      molestie lobortis lorem dignissim pretium.                    
                      <span class="signature-name">John Doe</span>                    
                      <span class="signature-from">from some company</span>
                  </div>
                </div>               
              </li> 
            </ul>                                   
          </div>
      </div>
    </div>
    <div class="download-area">
      <div class="question">Do you Like this app?</div>      
      <a href="#form" class="button download">DOWNLOAD NOW</a> 
        <form method="POST" class="form" action="ajax.php" id="form">
          <div class="form-name">Contact</div>
          <input type="text" name="name" class="name" placeholder="Your name" required accept-charset="utf-8"/>          
          <input type="email" name="email" class="email" placeholder="Your email" required accept-charset="utf-8"/>          
          <input type="text" name="number" class="number" placeholder="Your number" required accept-charset="utf-8"/>          
          <input type="text" name="subject" placeholder="Subject" accept-charset="utf-8"/>
          <textarea placeholder="Message" name="message" accept-charset="utf-8"></textarea>          
          <button type="submit" id="sub">Send</button>
        </form>    
    </div>
    <div class="map-area">
      <div class="container">
        <div id="map" style="width: 100%; height: 100%;"></div>
        <form method="POST" class="form" action="ajax.php">
          <div class="form-name">Contact</div>
          <input type="text" name="name" class="name" placeholder="Your name" required accept-charset="utf-8"/>          
          <input type="email" name="email" class="email" placeholder="Your email" required accept-charset="utf-8"/>          
          <input type="text" name="number" class="number" placeholder="Your number" required accept-charset="utf-8"/>          
          <input type="text" name="subject" placeholder="Subject" accept-charset="utf-8"/>
          <textarea placeholder="Message" name="message" accept-charset="utf-8"></textarea>          
          <button type="submit">Send</button>
        </form>  
        <div class="success">Форма успешно отправлена</div>  
      </div>   
    </div> -->
    <footer>
      <div class="container">
        <div class="copyright">Copyright © 2013 | BAZINGER | All Rights Reserved</div>
        <div class="privacy">
          <a href="#">Terms of Service</a>
          <div>|</div>
          <a href="#">Privacy Policy</a>
        </div>
      </div>
    </footer>
  <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
  </script>   
  <script type="text/javascript" src="assets/js/slick/slick.min.js"></script>
  <script type="text/javascript" src="assets/js/fancybox/jquery.fancybox.min.js"></script> 
  <script type="text/javascript" src="assets/js/jquery.inputmask.bundle.min.js"></script>
  <script
  src="https://api-maps.yandex.ru/2.1/?apikey=62edfbad-d465-4d6b-afb0-5c08db56210a&lang=ru_RU" type="text/javascript">
  </script>   
  <script src="assets/js/scripts.js"></script> 
  </body>
</html>