<?
include_once "config.php";

$name = $_POST['name'];
$email = $_POST['email'];
$number = $_POST['number'];
$sub = $_POST['subject'];
$msg = $_POST['message'];

$letter = 'имя пользователя: ' . $name . "\r\n" . 'e-mail пользователя: ' . $email . "\r\n" . 'телефон пользователя: ' . $number . "\r\n" . 'сообщение пользователя: ' . $msg;
$headers  = "Content-type: text/plain; charset=utf-8";

if (preg_match('/[0-9]/', $name) == 1) { 
	$error["name"] = "Не используйте в имени цифры!";	
}

if (preg_match('/[a-zA-Z]/', $name) == 1) { 	
	$error["name"] = "Используйте русские буквы в имени!";
}

if (preg_match('/[a-яёА-ЯЁa-zA-Z0-9]{2}/u', $name) == 0)  { 
	$error["name"] = "Имя не может состоять из 1 символа!";	
}

if (preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $email) == 0)  { 	
	$error["email"] = "Не корректно введен адрес почты!";	
}

if (preg_match('/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', $number) == 0)  { 		
	$error["number"] = "Не корректно введен номер телефона!";	
}

if ($error == 0) {
	$success = array("ok");   
	mail($db_email, $sub, $letter, $headers);
	$link = mysqli_connect($db_host, $db_user, $db_password, $db_base) or die ('ERROR' . mysqli_error($link));
	$query_insert = 'INSERT INTO messages (name, message, email, tel, subject) VALUES ("' . $name . '", "' . $msg . '", "' . $email . '", "' . $number . '", "' . $sub . '")';
	mysqli_query($link, $query_insert) or die ('ERROR' . mysqli_error($link));
	mysqli_close($link); 
} 

$arr = array($success, $error);	
echo json_encode($arr);

 ?>