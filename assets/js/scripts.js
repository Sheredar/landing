var button = $(".button.slider-download")

// button.on("click", function(e) {
//     alert("hello world")
//     e.preventDefault()
// })

$(document).on("click", ".button.slider-download", function(e) {
    alert("hello world");
    e.preventDefault();
})

$(document).ready(function(){
    $(".slider").slick({
        dots:true
    })     
});

$(".fancybox").fancybox({
	infobar : false,
    caption : function( instance, item ) {
        var caption = $(this).data('caption') || '';
        return ( caption.length ? caption + '<br />' : '' ) + 'SCREEN SHOT# <span data-fancybox-index></span>';
    }
});

$(".form").on("submit", sendForm);

function sendForm(e) {        
    var currentform = $(e.target).attr("class");
    e.preventDefault();    
    var data = $(".form").serialize();  
    $.ajax({
        url:"ajax.php",        
        type:"POST",
        data: data,
        success: function(data) {   
            var ajaxanswer = JSON.parse(data);  
            var ajaxsuccess = ajaxanswer[0];
            var ajaxerror = ajaxanswer[1];          
            if (ajaxsuccess == "ok") {                
                $(e.target)[0].reset();
                $(e.target).css('display', 'none');
                var height = $(".form").height();
                if ($(e.target).attr("class") == "form") {
                    $(".success").css('display','flex');
                    $(".success").height(height);  
                } else {
                    $.fancybox.close();
                }                
            } else { 
                $(e.target).find("span").remove();
                $(e.target).find("input").removeClass("active");                
                for (var inputname in ajaxerror) {  
                    $(e.target).find('.' + inputname).addClass("active");                    
                    $(e.target).find('.' + inputname).after("<span>" + ajaxerror[inputname] + "</span>"); 
                }                              
            }           
        }
    }); 
}

$(document).ready(function(){
  $(".number").inputmask("+7(999)999-99-99");  
});

$(document).ready(function(){
    $(".button.download").fancybox();    
});

ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [56.751574, 38.573856],
            zoom: 9
        }, {
            searchControlProvider: 'yandex#search'
        });

    var placemark = new ymaps.Placemark([56.491574, 39.673856], {}, {      
        preset: "islands#circleDotIcon",       
        iconColor: '#ff0000'
    });

    myMap.geoObjects.add(placemark);
});



